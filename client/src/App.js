import "bootstrap/dist/css/bootstrap.min.css";
import { Fragment, useState } from "react";
import Filter from "./components/Filter/Filter";
import List from "./components/List/List";

function App() {
  const [players, setPlayers] = useState([
    {
      id: 1,
      username: "a",
      email: "a@gmail.com",
      experience: 5,
      level: 3,
    },
    {
      id: 2,
      username: "b",
      email: "b@gmail.com",
      experience: 5,
      level: 9,
    },
    {
      id: 3,
      username: "c",
      email: "c@gmail.com",
      experience: 12,
      level: 3,
    },
  ]);

  const [filteredPlayers, setFilteredPlayers] = useState(players);
  const [screen, setScreen] = useState("LIST");

  const filterHandler = (params) => {
    console.log(params);
    if (!params.username && !params.email && !params.experience && !params.level) {
      setFilteredPlayers(players);
      return;
    }
    const newFilteredPlayers = players.filter((player) => {
      if (params.username && player.username !== params.username) return false;
      if (params.email && player.email !== params.email) return false;
      if (params.experience && player.experience != params.experience) return false;
      if (params.level && player.level != params.level) return false;
      return true;
    });
    console.log(newFilteredPlayers);
    setFilteredPlayers(newFilteredPlayers);
  };

  const [fields, setFields] = useState({
    username: "",
    email: "",
    experience: "",
    level: "",
  });

  const changeHandler = (fields, value) => {
    const newFields = { ...fields };
    newFields[fields] = value;

    setFields(newFields);
  };

  const submitHandler = () => {
    const newPlayer = {
      id: players.length + 1,
      ...fields,
    };
    const newPlayers = [...players, newPlayer];
    setPlayers(newPlayers);
    setFilteredPlayers(newPlayers);
    setScreen("LIST");
  };

  return (
    <div className="container pt-5">
      {screen === "LIST" ? (
        <Fragment>
          <div className="d-flex justify-content-between bg-secondary p-2 mx-0 bg-opacity-25 border rounded-2">
            <h1>Players</h1>
            <button className="btn btn-primary" onClick={() => setScreen("TAMBAH")}>
              Tambah
            </button>
          </div>
          <div className="row">
            <div className="col-3 bg-secondary p-2 bg-opacity-10 my-5 mx-3 border rounded-2">
              <Filter onFilter={filterHandler} test={2} />
            </div>
            <div className="col bg-secondary p-2 bg-opacity-10 my-5 mx-3 border rounded-2">
              <List data={filteredPlayers} />
            </div>
          </div>
        </Fragment>
      ) : null}

      {screen === "TAMBAH" && (
        <Fragment>
          <h1 className="text-center p-3 mx-3 bg-secondary bg-opacity-25 my-5 border rounded-2">Tambah Player</h1>
          <div className="row p-3 mx-3 bg-secondary bg-opacity-10 my-5 border rounded-2 justify-content-center">
            <div className="col-6">
              <div className="mb-3">
                <label className="form-label">Username</label>
                <input className="form-control" value={fields.username} onChange={(e) => changeHandler("username", e.target.value)} />
              </div>
              <div className="mb-3">
                <label className="form-label">Email</label>
                <input className="form-control" value={fields.email} onChange={(e) => changeHandler("email", e.target.value)} />
              </div>
              <div className="mb-3">
                <label className="form-label">Experience</label>
                <input className="form-control" value={fields.experience} onChange={(e) => changeHandler("experience", e.target.value)} />
              </div>
              <div className="mb-3">
                <label className="form-label">Level</label>
                <input className="form-control" value={fields.level} onChange={(e) => changeHandler("level", e.target.value)} />
              </div>
              <button className="btn btn-primary mx-1" onClick={() => setScreen("LIST")}>
                Back
              </button>
              <button className="btn btn-primary" onClick={submitHandler}>
                Submit
              </button>
            </div>
          </div>
        </Fragment>
      )}
    </div>
  );
}

export default App;
