import React, { useState } from "react";

export default function Filter(props) {
  const [fields, setFields] = useState({
    username: "",
    email: "",
    experience: "",
    level: "",
  });

  const changeHandler = (fields, value) => {
    const newFields = { ...fields };
    newFields[fields] = value;

    setFields(newFields);
  };

  return (
    <div>
      <div className="mb-3">
        <label className="form-label">Username</label>
        <input className="form-control" value={fields.username} onChange={(e) => changeHandler("username", e.target.value)} />
      </div>
      <div className="mb-3">
        <label className="form-label">Email</label>
        <input className="form-control" value={fields.email} onChange={(e) => changeHandler("email", e.target.value)} />
      </div>
      <div className="mb-3">
        <label className="form-label">Experience</label>
        <input className="form-control" value={fields.experience} onChange={(e) => changeHandler("experience", e.target.value)} />
      </div>
      <div className="mb-3">
        <label className="form-label">Level</label>
        <input className="form-control" value={fields.level} onChange={(e) => changeHandler("level", e.target.value)} />
      </div>
      <button className="btn btn-primary" onClick={() => props.onFilter(fields)}>
        Submit
      </button>
    </div>
  );
}
